/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx  = 1;        /* border pixel of windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const int showbar            = 0;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const Bool viewontag         = True;     /* Switch view on tag switch */
static const char *fonts[]          = { "monofur:size=12" };
static const char col_gray1[]       = "#0D0E16";
static const char col_gray2[]       = "#0D0E16";
static const char col_gray3[]       = "#A49B65";
static const char col_gray4[]       = "#0D0E16";
static const char col_cyan[]        = "#A49B65";
static const char *colors[][3]      = {
	/*               fg         bg         border   */
	[SchemeNorm] = { col_gray3, col_gray1, col_gray2 },
	[SchemeSel]  = { col_gray4, col_gray3,  col_cyan  },
};


//scratchpad
typedef struct {
	const char *name;
	const void *cmd;
} Sp;
const char *spcmd1[] = {"st", "-n", "spterm", "-g", "85x20", NULL };
const char *spcmd2[] = {"st", "-n", "spfm", "-g", "90x20", "-e", "vifm", NULL };
static Sp scratchpads[] = {
	/* name          cmd  */
	{"spterm",      spcmd1},
	{"spfm",    spcmd2},
};




/* tagging */
//static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };
static const char *tags[] = { "", "", "", "", "", "", "" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class          instance              title       tags mask     iscentered     isfloating   monitor */


	{ NULL,		  "spterm",		NULL,		SPTAG(0),		1,			 -1 },
	{ NULL,		  "spfm",		NULL,		SPTAG(1),		1,			 -1 },



    { "firefox",            NULL,       NULL,       1 << 1,       0,           0,           -1 },
    { "LibreWolf",          NULL,       NULL,       1 << 1,       0,           0,           -1 },
    { "qutebrowser",        NULL,       NULL,       1 << 1,       0,           0,           -1 },
    { "Vieb",               NULL,       NULL,       1 << 1,       0,           1,           -1 },
    { "xdman-Main", 	    NULL,       NULL,       1 << 2,       0,           1,           -1 }, 
    { "Virt-manager",       NULL,       NULL,       0 << 3,       1,           0,           -1 }, 
    { "Gimp",               NULL,       NULL,       1 << 5,       0,           0,           -1 }, 
    { "Inkscape",           NULL,       NULL,       1 << 5,       0,           0,           -1 }, 
    { "stacer",             NULL,       NULL,       1 << 6,       1,           1,           -1 }, 

    { "Pavucontrol",        NULL,       NULL,       0 << 6,       1,           1,           -1 }, 
    { "scrcpy",        	    NULL,       NULL,       0 << 6,       1,           1,           -1 }, 
};

/* layout(s) */
static const float mfact     = 0.6; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */
static const int lockfullscreen = 1; /* 1 will force focus on the fullscreen window */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[┼]=",      tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
};

/* key definitions */
#define MODKEY Mod1Mask
#define TAGKEYS(CHAIN,KEY,TAG) \
	{ MODKEY,                       CHAIN,    KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           CHAIN,    KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             CHAIN,    KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, CHAIN,    KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", NULL };
static const char *termcmd[]  = { "st", NULL };

static const Key keys[] = {
	/* modifier                     chain key   key        function        argument */

	TAGKEYS(                        -1,         XK_1,                      0)
	TAGKEYS(                        -1,         XK_2,                      1)
	TAGKEYS(                        -1,         XK_3,                      2)
	TAGKEYS(                        -1,         XK_4,                      3)
	TAGKEYS(                        -1,         XK_5,                      4)
	TAGKEYS(                        -1,         XK_6,                      5)
	TAGKEYS(                        -1,         XK_7,                      6)

	{ MODKEY,                       -1,         XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ControlMask,           -1,         XK_0,      tag,            {.ui = ~0 } },

	{ MODKEY,             		-1,         XK_j,      focusstack,     {.i = +1 } },
	{ MODKEY,                     	-1,	    XK_k,      focusstack,     {.i = -1 } },
	{ MODKEY,                      	-1,	    XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY,                	-1, 	    XK_l,      setmfact,       {.f = +0.05} },

	{ MODKEY,			-1,	    XK_b,      view,	       {0} },

	{ MODKEY,                       -1,         XK_q,      killclient,     {0} },

	{ MODKEY,		        -1,	    XK_Return, spawn,	       {.v = termcmd } },

	{ MODKEY,			-1,    	    XK_Tab,    setlayout,      {.v = &layouts[0]} },
	{ MODKEY|ShiftMask,    	       	-1,         XK_Tab,    setlayout,      {.v = &layouts[1]} },
	{ MODKEY|ControlMask,		-1,         XK_Tab,    setlayout,      {.v = &layouts[2]} },

    	{ MODKEY|ControlMask,		-1, 	    XK_j,      incnmaster,     {.i = +1 } },
	{ MODKEY|ControlMask,		-1,    	    XK_k,      incnmaster,     {.i = -1 } },
	



	{ MODKEY,                       -1,         XK_f,      fullscreen,     {0} },



	{ MODKEY|ControlMask,		-1,   	    XK_space,  togglebar,      {0} },

    	{ MODKEY|ShiftMask,		-1,         XK_h,       shiftview,     { .i = -1 } },
	{ MODKEY|ShiftMask,     	-1,         XK_l,       shiftview,     { .i = 1 } },
    	{ MODKEY|ControlMask,   	-1,    	    XK_h,       shiftview2all, { .i = -1 } },
	{ MODKEY|ControlMask,   	-1,	    XK_l,       shiftview2all, { .i = 1 } },



	{ MODKEY,			-1,	    XK_z,       spawn,	       SHCMD("qutebrowser") },
	{ MODKEY|ShiftMask,		-1,	    XK_z,       spawn, 	       SHCMD("librewolf") },

	{ MODKEY|ShiftMask,		-1,         XK_p,       spawn,         SHCMD("pavucontrol") },

	{ MODKEY,                       XK_e,       XK_e,       spawn,         SHCMD("emacsclient -c -a 'emacs'") },
	{ MODKEY,                       XK_e,       XK_b,       spawn,         SHCMD("emacsclient -c -a 'emacs' --eval '(ibuffer)'") },
	{ MODKEY,                       XK_e,       XK_d,       spawn,         SHCMD("emacsclient -c -a 'emacs' --eval '(dired nil)'") },
	{ MODKEY,                       XK_e,       XK_n,       spawn,         SHCMD("emacsclient -c -a 'emacs' --eval '(elfeed)'") },
	{ MODKEY,                       XK_e,       XK_s,       spawn,         SHCMD("emacsclient -c -a 'emacs' --eval '(eshell)'") },

	{ MODKEY,              	      	XK_d,       XK_d,       spawn,         {.v = dmenucmd } },
    	{ MODKEY,	        	XK_d,       XK_m,       spawn,	       SHCMD("mutt_switch_d") },
	{ MODKEY,   	                XK_d,       XK_t,       spawn,         SHCMD("todo") }, 
	{ MODKEY,	              	XK_d,       XK_a,       spawn,	       SHCMD("mount_android_d") },
	{ MODKEY,	   	        XK_d,       XK_s,       spawn,	       SHCMD("maim_d") },
    	{ MODKEY,             	        XK_d,       XK_p,       spawn,         SHCMD("morc_menu") },
	{ MODKEY,			XK_d,       XK_n,       spawn,         SHCMD("networkmanager_d") },
    	{ MODKEY,               	XK_d,       XK_c,       spawn,         SHCMD("configs_d") },
	{ MODKEY,   			XK_d,	    XK_x,       spawn,         SHCMD("redprompt \"Do you want to shutdown?\" \"shutdown -h now\"") },
    	{ MODKEY,              		XK_d,	    XK_space,   spawn,         SHCMD("keyboard-layout") },



    	{ MODKEY|ControlMask,           XK_x,	    XK_c,       spawn,         SHCMD("killall dwm") },

	{ MODKEY,               	-1,	    XK_Escape,  spawn,         SHCMD("xkill") }, 
	{ MODKEY|ShiftMask,		-1,   	    XK_r,	quit,          {1} },
	{ MODKEY,	      		-1,	    XK_space,	zoom,	       {0} },
	{ MODKEY|ShiftMask,		-1,   	    XK_space,	togglefloating,{0} },
	
	//------???
	{ MODKEY,               	-1,	    XK_comma,  	focusmon,      {.i = -1 } },
	{ MODKEY,               	-1,    	    XK_period, 	focusmon,      {.i = +1 } },
	{ MODKEY|ShiftMask,   	  	-1,	    XK_comma,  	tagmon,        {.i = -1 } },
	{ MODKEY|ShiftMask,   	  	-1,	    XK_period, 	tagmon,        {.i = +1 } },

	//{ 0,	                        -1,         XF86XK_AudioMute,	        spawn,    SHCMD("amixer sset Master toggle ; pkill -RTMIN+10 dwmblocks|notify-send \"VOLUME >>> mute \"") },
	//{ 0,		                -1,         XF86XK_AudioRaiseVolume,	spawn,    SHCMD("amixer sset Master 5%+ ; pkill -RTMIN+10 dwmblocks|notify-send \"VOLUME >>> +5\"") },
	//{ 0,	                        -1,         XF86XK_AudioLowerVolume,	spawn,    SHCMD("amixer sset Master 5%- ; pkill -RTMIN+10 dwmblocks|notify-send \"VOLUME >>> -5\"") },





	
	{ MODKEY,            	        XK_t,	            XK_t,       togglescratch,  {.ui = 0 } },
	{ MODKEY,                    	XK_t,   	    XK_j,       togglescratch,  {.ui = 1 } },

 };

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static const Button buttons[] = {
	/* click                event mask      button          function        argument 
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button1,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
*/

	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },

	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },

};

